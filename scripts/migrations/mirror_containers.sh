#!/bin/bash
#
###
# Usage:
#    
#    ./mirror_containers.sh kics registry.gitlab.com/gitlab-org/security-products/analyzers registry.gitlab.com/security-products
#
# Note: ensure you have the proper permissions to push to the target registry
###

sca_analyzers="bundler-audit gemnasium gemnasium-maven gemnasium-python license-finder retire.js"
sast_analyzers="bandit brakeman eslint flawfinder gosec kics kubesec mobsf nodejs-scan phpcs-security-audit pmd-apex security-code-scan semgrep sobelow spotbugs"

# count_tags accepts a space-separated list of analyzer directories and integer (MAJOR version).
# It iterates over each directory and counts the total occurrences of the major tag.
#
#   Usage: `count_tags "bundler-audit gemnasium gemnasium-maven gemnasium-python retire.js" 2`
#
count_tags() {
  count=0
  for a in $1
  do
    if [ -z "$2" ]; then
      number=$(grep "## " $a/CHANGELOG.md | wc -l)
      printf "%-20s %-5s\n" $a $number
      count=$((number + count))
    else
      number=$(grep "## v$2" $a/CHANGELOG.md | wc -l)
      printf "%-20s %-5s\n" $a $number
      count=$((number + count))
    fi
  done

  echo "total tags: $count"
  echo 
}

# copy_major accepts an analyzer directory, old registry location, and new registry location.
# It counts the total major, minor, and patch tags and after confirmation, iterates over
# each, pulling from the old location and pushing to the new location.
#
# copy_major assumes current user is already authenticated to new registry location.
#
#   Usage: `copy_major kics registry.gitlab.com/gitlab-org/security-products/analyzers registry.gitlab.com/security-products`
#
copy_major() {
  a=$1
  old_location=$2
  new_location=$3

  major_header=$(grep "## v" $a/CHANGELOG.md | head -n1 | sed -E 's/\..+//g')

  major=$(echo $major_header | sed -E 's/## v//g')

  minor_tags=$(grep "$major_header" $a/CHANGELOG.md | rev | cut -d. -f2- | rev | cut -dv -f2 | uniq)
  patch_tags=$(grep "$major_header" $a/CHANGELOG.md | cut -dv -f2 | uniq)

  major_count=1
  minor_count=$(echo "$minor_tags" | wc -l)
  patch_count=$(echo "$patch_tags" | wc -l)
  total_count=$((major_count + minor_count + patch_count))
  
  echo "major: $major"
  echo "minor_tags: $minor_count"
  echo "$minor_tags"
  echo "patch_tags: $patch_count"
  echo "$patch_tags"
  echo "total tags: $total_count"
  echo

  echo "------------------------------------------"
  echo "Preparing to pull and push the following minor tags..."
  for t in $minor_tags
  do
    echo "From: $old_location/$a:$t"
    echo "To: $new_location/$a:$t"
    echo
  done

  read -p "Press Y/y to confirm..." -n 1 -r
  if [[ ! $REPLY =~ ^[Yy]$ ]]
  then
    exit 1
  fi

  echo
  echo "------------------------------------------"
  echo "Mirroring tags..."
  for t in $minor_tags
  do
    pull_and_push "$old_location/$a:$t" "$new_location/$a:$t"
  done

  echo "------------------------------------------"
  echo "Preparing to pull and push the following patch tags..."
  for t in $patch_tags
  do
    echo "From: $old_location/$a:$t"
    echo "To: $new_location/$a:$t"
    echo
  done

  read -p "Press Y/y to confirm..." -n 1 -r
  if [[ ! $REPLY =~ ^[Yy]$ ]]
  then
    exit 1
  fi

  echo
  echo "------------------------------------------"
  echo "Mirroring tags..."
  for t in $patch_tags
  do
    pull_and_push "$old_location/$a:$t" "$new_location/$a:$t"
  done

  return
}

pull_and_push() {
  docker pull "$1"
  if [ "$?" -ne 0 ]; then
    echo
    echo "*****************************************"
    echo "ERROR: Failed to pull $1"
    echo "*****************************************"
    echo
    return
  fi
  docker tag "$1" "$2"
  docker push "$2"
  echo
}

copy_major $1 $2 $3
