ARG IMAGE_NAME

FROM "$IMAGE_NAME"

ARG GO_VERSION

# Force the root user so we don't run into permissions problems when
# installing dependencies. This is mainly for the FIPS-compliant RHEL
# images which declare ``USER gitlab`.
USER root

# Handle distro-specific setup.
COPY ./distro-setup.sh .
RUN ./distro-setup.sh

# Install Go
RUN mkdir -p /go && \
      GO_DOWNLOAD=$(uname -a | grep -q "x86_64" && echo "$GO_VERSION.linux-amd64.tar.gz" || echo "$GO_VERSION.linux-arm64.tar.gz") && \
      wget -qO- https://golang.org/dl/$GO_DOWNLOAD | tar -C /usr/local -xz
ENV GOBIN="/usr/local/go/bin"
ENV PATH="${GOBIN}:${PATH}"

RUN go install github.com/go-delve/delve/cmd/dlv@latest

CMD ["sh"]
