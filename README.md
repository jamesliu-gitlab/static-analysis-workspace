# Static Analysis Workspace

## First run

```command
$ git submodule update --init --recursive
```

## Fetch latest

```command
$ git submodule update --recursive --remote
```
